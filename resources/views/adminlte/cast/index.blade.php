@extends('adminlte.master')

@section('content')

<div class="card">
    <div class="card-header">
      <div class="d-flex justify-content-between">
        <h1 class="card-title">Cast Table</h1>
        <a href="/cast/create" class="btn btn-success">Create New Cast</a>
      </div>
    </div>
    <!-- /.card-header -->
    
    <div class="card-body">
      <table id="example1" class="table table-bordered">
        <thead>
          <tr>
            <th style="width: 10px">#</th>
            <th>Nama</th>
            <th>Umur</th>
            <th style="width: 40px">Action</th>
          </tr>
        </thead>
        <tbody>
          @forelse($posts as $key => $post)
          <tr>
            <td>{{$key+1}}</td>
            <td>{{$post->nama}}</td>
            <td>{{$post->umur}}</td>
            <th>
              <div class="btn-group" role="group" aria-label="Basic outlined example">
                <a href="/cast/{{$post->id}}" type="button" class="btn btn-info">Show</a>
                <a href="/cast/{{$post->id}}/edit" type="button" class="btn btn-warning">Edit</a>
                <form action="/cast/{{$post->id}}" method="post">
                  @csrf
                  @method('DELETE')
                  <input type="submit" value="Delete" class="btn btn-danger"/>
                </form>
              </div>
            </th>
          </tr>
          @empty
            <tr>
              <td  colspan="4" >
                <h5 style="text-align:center">Cast Table is Empty</h5>
              </td>
            </tr>
          @endforelse
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection