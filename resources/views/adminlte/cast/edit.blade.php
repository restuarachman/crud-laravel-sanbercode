@extends('adminlte.master')
@section('content')
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">Edit Cast</h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
  <form role="form" action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="card-body">
      <div class="form-group">
        <label for="exampleInputEmail1">Nama Lengkap</label>
        <input type="text" class="form-control" value="{{$cast->nama}}" name="nama" id="nama" placeholder="Enter ...">
        @error('nama')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      <div class="form-group">
        <label for="exampleInputPassword1">Umur</label>
        <input type="number" class="form-control" value="{{$cast->umur}}" name="umur" id="umur"placeholder="Enter ...">
        @error('umur')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      <div class="form-group">
        <label>Bio</label>
          <textarea class="form-control" rows="3" placeholder="Enter ..." name="bio" id="bio">{{$cast->bio}}</textarea>
          @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
      </div>
    </div>
    <!-- /.card-body -->
    <div>
      <button type="submit" class="btn btn-primary">Update</button>
    </div>
  </form>
</div>
@endsection('content')