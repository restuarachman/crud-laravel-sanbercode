<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function index() {
        $posts = DB::table('cast')->get();
        return view('adminlte.cast.index', compact('posts'));
    }

    public function create() {
        return view('adminlte.cast.create');
    }

    public function store(Request $req) {
        $req->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $req["nama"],
            "umur" => (int) $req["umur"],
            "bio" => $req["bio"]
        ]);

        return redirect('/cast');
    }

    public function show($id) {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('adminlte.cast.show', compact('cast'));
    }

    public function edit($id) {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('adminlte.cast.edit', compact('cast'));
    }
    public function update($id, Request $req)
    {
        $req->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $req["nama"],
                'umur' => $req["umur"],
                'bio' => $req["bio"],
            ]);
        return redirect('/cast');
    }

    public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
